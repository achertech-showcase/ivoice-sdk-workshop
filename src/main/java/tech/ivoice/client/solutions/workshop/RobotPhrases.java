package tech.ivoice.client.solutions.workshop;

import tech.ivoice.platform.sdk.messages.Session;
import tech.ivoice.platform.sdk.messages.common.PlaySource;
import tech.ivoice.platform.sdk.messages.common.driver_options.GoogleDriverOptions;
import tech.ivoice.platform.sdk.messages.session.Play;
import tech.ivoice.platform.sdk.messages.session.PlayCollectSpeech;

import static tech.ivoice.platform.sdk.messages.Messages.playCollectSpeech;
import static tech.ivoice.platform.sdk.messages.common.PlaySource.sequence;
import static tech.ivoice.platform.sdk.messages.common.PlaySource.speechSynthesis;

/**
 * Class contain description of speech to play in dialog.
 * <p>
 * Speech can be url of audio file, or it can be synthesized using TTS
 */
class RobotPhrases {

    private static final String ASR_LANG = "en-US";
    private static final GoogleDriverOptions TTS_OPTIONS = new GoogleDriverOptions(ASR_LANG, "en-US-Standard-D", 0.75);

    /**
     * @return method should return PlayCollectSpeech;
     * <p>
     * PlayCollectSpeech is SDK message, commanding platform to play message and collect answer
     */
    PlayCollectSpeech firstQuestion(Parameters parameters) {
        PlaySource playSource = speechSynthesis("Hello! Could you please take a moment to update your number in supermed clinic?", TTS_OPTIONS);
        return playCollectSpeech(playSource)
                .setDriver("google")
                .setLang(ASR_LANG)
                .setTimeout(10)
                // when collect should start, before phrase ends; in this case:
                // start collect 2 seconds early from request speech finished
                .startCollectBeforePlayEnd(2)
                // if client answering words not included in synonyms, how long should robot wait after last word, to decide that phrase ended
                .setSilenceAfterSpeechTimeout(3)
                // how long robot will wait until it will finish collection with "silence" result
                .setWaitingInputTimeout(3)
                // here could be set input as SPEECH and DTMF if needed
                .setInput(PlayCollectSpeech.InputType.SPEECH)
                .build();
    }

    //Вы являетесь пациентом нашей клиники. И мы приготовили для Вас индивидуальный бонус.   <предложение, скидка, подарок и т.д.>.
    //Если вы хотите воспользоваться бонусом, скажите ДА.
    PlayCollectSpeech secondQuestion(Parameters parameters) {
        PlaySource playSource = sequence(
                speechSynthesis("is this number: ", TTS_OPTIONS),
                speechSynthesis(parameters.getPhone(), TTS_OPTIONS),
                speechSynthesis(" is your main phone number?", TTS_OPTIONS)
        );
        return playCollectSpeech(playSource)
                .setDriver("google")
                .setLang(ASR_LANG)
                .setTimeout(10)
                .startCollectBeforePlayEnd(2)
                .setSilenceAfterSpeechTimeout(3)
                .setWaitingInputTimeout(3)
                .setInput(PlayCollectSpeech.InputType.SPEECH)
                .build();
    }

    Play byePhrase() {
        return new Play(new Session(), speechSynthesis("Thank you for your time! Bye!", TTS_OPTIONS));
    }

}
