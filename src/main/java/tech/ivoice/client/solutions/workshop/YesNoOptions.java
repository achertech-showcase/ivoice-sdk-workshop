package tech.ivoice.client.solutions.workshop;

import com.google.common.collect.ImmutableList;
import com.google.common.collect.ListMultimap;
import java.util.List;
import java.util.Map.Entry;
import tech.ivoice.platform.extension.sdk.components.request_option.Option;

@SuppressWarnings({"RedundantArrayCreation", "unchecked"})
public enum YesNoOptions implements Option {
    YES("yes", Option.multimap(new Entry[]{Option.entry("en-EN", new String[]{"yes", "sure", "ok", "fine"})})),
    NO("no", Option.multimap(new Entry[]{Option.entry("en-EN", new String[]{"no", "never", "fuck"})}));

    private final String id;
    private final ListMultimap<String, String> variants;

    private YesNoOptions(String id, ListMultimap<String, String> variants) {
        this.id = id;
        this.variants = variants;
    }

    public String getId() {
        return this.id;
    }

    public List<String> getLocalizedVariants(String locale) {
        return this.variants.get(locale);
    }

    public List<String> getDtmfCodes() {
        return ImmutableList.of();
    }
}