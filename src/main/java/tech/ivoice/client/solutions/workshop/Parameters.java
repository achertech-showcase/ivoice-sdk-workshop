package tech.ivoice.client.solutions.workshop;

import com.google.gson.Gson;
import org.apache.commons.lang.StringUtils;

/**
 * convenience class to represent scenario initial parameters message (json) as DTO
 */
public class Parameters {

    private String phone;

    public String getPhone() {
        return phone;
    }

    public Parameters() {
    }

    public static Parameters extractFromScenarioInitMessage(Object message) {
        Gson gson = new Gson();
        return gson.fromJson(gson.toJson(message), Parameters.class);
    }

    @Override
    public String toString() {
        return "Parameters{" +
                "phone='" + phone + '\'' +
                '}';
    }

    public boolean valid() {
        return !StringUtils.isEmpty(phone);
    }
}
