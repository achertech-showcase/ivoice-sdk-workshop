package tech.ivoice.client.solutions.workshop;

import com.google.common.collect.ImmutableList;
import tech.ivoice.platform.extension.sdk.components.request_option.common.YesNo;
import tech.ivoice.platform.extension.sdk.components.request_option.enumoption.EnumRequestOptionComponent;
import tech.ivoice.platform.extension.sdk.components.request_option.strategy.WriteSilenceToResultsStrategy;
import tech.ivoice.platform.extension.sdk.fsm_support.Platform;
import tech.ivoice.platform.fsm.FsmContext;
import tech.ivoice.platform.fsm.OutState;
import tech.ivoice.platform.fsm.State;
import tech.ivoice.platform.sdk.messages.Messages;
import tech.ivoice.platform.sdk.messages.session.Play;

import java.util.Collections;
import java.util.logging.Logger;

import static java.lang.String.format;
import static tech.ivoice.platform.extension.sdk.fsm_support.Tools.submitLink;

/**
 * Scenario described as FSM (finite state machine), as states and transitions.
 *
 * https://en.wikipedia.org/wiki/Finite-state_machine
 *
 */
@SuppressWarnings("FieldCanBeLocal")
class ScenarioFsm {

    private static final Logger log = Logger.getLogger(ScenarioFsm.class.getName());

    // locale to use for speech recognition
    private static final String LOCALE = "en-EN";

    // client will get results in the form <result name:result> key-value map:
    private static final String FIRST_QUESTION_RESULT_NAME = "firstQuestion";
    private static final String SECOND_QUESTION_RESULT_NAME = "secondQuestion";
    private static final String SILENCE_RESULT_VALUE = "silence";
    private static final String POSITIVE_RESULT_VALUE = "yes";
    private static final String NEGATIVE_RESULT_VALUE = "no";

    private RobotPhrases plays = new RobotPhrases();

    // boilerplate; copy and use this
    private final FsmContext<ScenarioFsm> context = FsmContext.of(this);
    private Platform platform;
    private Parameters parameters;

    /*
        here we use SDK high-level components; high-level components incapsulate low-level interaction with platform
        to solve common problem.

        EnumRequestOptionComponent allow to associate requested option with recognized synonyms

        In this scenario we will ask 2 question, each with 2 option (positive answer - yes / negative answer - no)
     */

    private EnumRequestOptionComponent<YesNo> firstQuestion;
    private EnumRequestOptionComponent<YesNo> secondsQuestion;

    /**
     * Scenario FSM constructor contains:
     * 1. Initialization of states;
     * 2. Describing possible transitions between states (linking, see submitLink below)
     *
     * After constuctor ends, entry state is entered (see next method below constructor)
     *
     * @param platform platform actor to send SDK messages requesting platform actions (asr, tts, saving results, etc)
     */
    ScenarioFsm(Platform platform) {
        // boilerplate - save platform here to send SDK messages to platform
        this.platform = platform;

        // components to collect options (high-level sdk language) are initialized:
        this.firstQuestion = new EnumRequestOptionComponent<>(platform,
                // question phrase:
                () -> plays.firstQuestion(parameters),
                // repeat of question if robot did not understood (skipped in this demo)
                Collections.emptyList(),
                YesNo.class,
                LOCALE,
                FIRST_QUESTION_RESULT_NAME,
                WriteSilenceToResultsStrategy.DO_NOTHING,
                SILENCE_RESULT_VALUE);

        this.secondsQuestion = new EnumRequestOptionComponent<>(platform,
                () -> plays.secondQuestion(parameters),
                Collections.emptyList(),
                YesNo.class,
                LOCALE,
                SECOND_QUESTION_RESULT_NAME,
                WriteSilenceToResultsStrategy.DO_NOTHING,
                SILENCE_RESULT_VALUE);

        // linking collected option with transition to next state

        submitLink(
                // in case we get positive answer on first question
                firstQuestion.option(YesNo.YES),
                // perform transition to second question state
                secondsQuestion.start,
                platform,
                // save result under this name for report
                FIRST_QUESTION_RESULT_NAME,
                // save this value for result
                POSITIVE_RESULT_VALUE);

        submitLink(firstQuestion.option(YesNo.NO), playBye, platform, FIRST_QUESTION_RESULT_NAME, NEGATIVE_RESULT_VALUE);
        submitLink(firstQuestion.fail, playBye, platform, FIRST_QUESTION_RESULT_NAME, SILENCE_RESULT_VALUE);
        submitLink(firstQuestion.unknown, playBye, platform, FIRST_QUESTION_RESULT_NAME, "do not understand");

        submitLink(secondsQuestion.option(YesNo.YES), playBye, platform, SECOND_QUESTION_RESULT_NAME, POSITIVE_RESULT_VALUE);
        submitLink(secondsQuestion.option(YesNo.NO), playBye, platform, SECOND_QUESTION_RESULT_NAME, NEGATIVE_RESULT_VALUE);
        submitLink(secondsQuestion.fail, playBye, platform, SECOND_QUESTION_RESULT_NAME, SILENCE_RESULT_VALUE);
        submitLink(secondsQuestion.unknown, playBye, platform, "do not understand");

    }

    /**
     * first (entry) state. onEnter method is executed after scenario is initialized by platform - this is defined in ScenarioBundle
     *
     * Platform passes "message" object as argument, message contain any parameters from backend, needed to perform dialog
     */
    @SuppressWarnings("unchecked")
    State<ScenarioFsm> entry = context.state("entry")
            .onEnter((message, state, states, actions) -> {
                // parameters could be initiated here; message is passed as input from platform on scenario initialization
                this.parameters = Parameters.extractFromScenarioInitMessage(message);
                log.info("parameters extracted: " + parameters);

                // example of hangup action, in case if parameters are not valid to perform dialog
                // here could also be checks for client's API availability
                if (!parameters.valid()){
                    // 1. platform.accept is SDK action to save result. Client will see this in report on call result
                    platform.accept(Messages.result("error", "wrong parameters"));
                    // 2. performing transition to hangup

                    // action.redirect is SDK action, performing transition in the next state
                    return actions.redirect(states.justHangup);
                } else {
                    return actions.redirect(states.firstQuestion.start);
                }

                // list of possible transitions from started state
            }).transitions(sc -> ImmutableList.of(sc.firstQuestion.start, sc.justHangup)).build();

    /**
     * this is final state, playing bye phrase
     */
    private State<ScenarioFsm> playBye = context.state("playBye")
            .onEnter((message, state, states) -> platform.accept(plays.byePhrase()))
            .onProcess((message, state, states) -> {
                if (message instanceof Play.Success) {
                    return states.justHangup;
                } else if (message instanceof Play.Fail) {
                    platform.accept(Messages.log(((Play.Fail) message).getCause()));
                    return states.justHangup;
                } else
                    return state;
            }).build();

    private State<ScenarioFsm> justHangup = context.state("justHangup")
            .onEnter((message, state, states, actions) -> {
                platform.accept(Messages.hangup());
                return actions.redirect(states.finish);
            }).build();

    private OutState<ScenarioFsm> finish = context.outState("finish", false);

}
