package tech.ivoice.client.solutions.workshop;

import tech.ivoice.platform.extension.sdk.fsm_support.FsmScenario;
import tech.ivoice.platform.extension.sdk.fsm_support.Platform;
import tech.ivoice.platform.extension.sdk.fsm_support.components.wait_init.WaitInitComponent;
import tech.ivoice.platform.fsm.OutState;

import static tech.ivoice.platform.extension.sdk.fsm_support.Tools.link;

/**
 * boilerplate code:
 *
 * 1. give scenario unique name; use this name to call scenario
 *
 * 2. bootstrap scenario
 */
@tech.ivoice.platform.sdk.annotations.Scenario("plworkshop")
public class ScenarioBundle extends FsmScenario {

    @Override
    public void initialize(Platform platform, OutState<WaitInitComponent> outState) {
        ScenarioFsm component = new ScenarioFsm(platform);
        link(outState, component.entry);
    }

}
